# typescript-jest-mock

This project is **deprecated** and has been moved to [jest-mock-object](https://gitlab.com/mblanchet528/jest-mock-object#readme).

### Install
`npm install jest-mock-object --save-dev` 

### Documentation
Please refer to `jest-mock-object` project documentation [here](https://gitlab.com/mblanchet528/jest-mock-object#readme).