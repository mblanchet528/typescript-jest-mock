"use strict";

import { any, eq } from "./../src/matchers";

const VALUE: any = 5;

test("given any value when using any matcher then return true", () => {
    let matcherFunction: Function = any();
    expect(matcherFunction(VALUE)).toEqual(true);
})

test("given 2 equal values when using eq matcher then return true", () => {
    let matcherFunction: Function = eq(VALUE);
    expect(matcherFunction(VALUE)).toEqual(true);
})

test("given 2 different values when using eq matcher then return false", () => {
    let matcherFunction: Function = eq(VALUE);
    expect(matcherFunction(VALUE + 1)).toEqual(false);
})