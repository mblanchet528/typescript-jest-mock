"use strict";

import { Mock } from "./../src/mock";

let mockObj;

test("given a type when I create a mock using its prototype then each functions are mocked", () => {
    mockObj = Mock.fromPrototype(TestClass.prototype);

    expect(mockObj.setX._isMockFunction).toEqual(true);
    expect(mockObj.getY._isMockFunction).toEqual(true);
})

test("given a type when I create a mock using its prototype then dont mock constructor", () => {
    mockObj = Mock.fromPrototype(TestClass.prototype);
    expect(mockObj.constructor.mock).toBeUndefined();    
})

test("when I create a mock from function names then create a mock having specified mocked functions", () => {
    mockObj = Mock.fromFunctionNames(["execute", "cancel"]);

    expect(mockObj.execute._isMockFunction).toEqual(true);
    expect(mockObj.cancel._isMockFunction).toEqual(true);
})

test("when a mock is created then each mocke function can return value according to argument matchers", () => {
    mockObj = Mock.fromPrototype(TestClass.prototype);
    
    expect(mockObj.setX.mockReturnValueWithArgs).toBeTruthy();
    expect(mockObj.getY.mockReturnValueWithArgs).toBeTruthy();
})

test("when mocking return value according to args then use equal as default matcher", () => {
    mockObj = Mock.fromFunctionNames(["evaluate"]);
    mockObj.evaluate.mockReturnValueWithArgs([123], 456);

    let result = mockObj.evaluate(123);

    expect(result).toEqual(456);
})

test("when mocking return value according to args then return undefined if args not matching", () => {
    mockObj = Mock.fromFunctionNames(["evaluate"]);
    mockObj.evaluate.mockReturnValueWithArgs([123], 456);

    let result = mockObj.evaluate(234);

    expect(result).toBeUndefined();
})

test("given return value defined when mocking return value according to args then return default value", () => {
    mockObj = Mock.fromFunctionNames(["evaluate"]);
    mockObj.evaluate.mockReturnValue(789);
    mockObj.evaluate.mockReturnValueWithArgs([123], 456);

    let result = mockObj.evaluate(123);

    expect(result).toEqual(789);
})

test("given a custom matcher when mocking resturn value according to args then return specified value if args matching", () => {
    let isEven = (x) => { return (x % 2) === 0 };
    mockObj = Mock.fromFunctionNames(["evaluate"]);
    mockObj.evaluate.mockReturnValueWithArgs([isEven], 456);

    let resultEven = mockObj.evaluate(2);
    let resultOdd = mockObj.evaluate(3);

    expect(resultEven).toEqual(456);
    expect(resultOdd).toBeUndefined();
})

class TestClass {
    
    private x: number;
    private y: string;

    constructor (x: number, y: string) {
        this.x = x;
        this.y = y;
    }

    public setX(x: number) {
        this.x = x;
    }

    public getY(): string {
        return this.y;
    }
}