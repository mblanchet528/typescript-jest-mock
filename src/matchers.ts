"use strict";

export function any(): Function{
    return () => (true);
}

export function eq(v: any): Function {
    return ((a: any, b: any) => (a === b)).bind(null, v);
}