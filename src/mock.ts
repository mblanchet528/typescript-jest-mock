"use strict";

import { eq } from "./matchers";

export class Mock {

    public static fromPrototype<T>(prototype: any): T {
        function isFunction(property: any): boolean {
            return typeof property === "function";
        }

        let functions: string[] = Object.getOwnPropertyNames(prototype)
            .filter((p) => (isFunction(prototype[p])))
            .filter((p) => (p !== "constructor"));
        return <T> this.fromFunctionNames(functions);        
    }

    public static fromFunctionNames(functionNames: string[]): any {
        let mock: any = {};
        functionNames.forEach((fn) => mock[fn] = this._createMockFunction());
        return mock;
    }

    private static _createMockFunction() {
        let expectedCalls: Map<any[], any> = new Map();
        let fn: any = jest.fn();
        fn.mockImplementation(this._handleCall.bind(null, expectedCalls, fn.mock.calls));
        fn["mockReturnValueWithArgs"] = this._mockReturnValueWithArgs.bind(null, expectedCalls);
        return fn;
    }

    private static _handleCall(expectedCalls: Map<any[], any>, calls: any[][]): any {
        let lastCall = calls[calls.length - 1];
        let result;
        expectedCalls.forEach((value, key, map) => {
            if (lastCall.length === key.length) {
                let matches: boolean = true;
                for (let i = 0; i < key.length; i++) {
                    let matcher: any = key[i];
                    if (!(matcher(lastCall[i]))) {
                        matches = false;
                        break;
                    }
                }
                if (matches) {
                    result = value;
                    return;
                }
            }
        });
        if (result) {
            return result;
        }
    }

    private static _mockReturnValueWithArgs(expectedCalls: Map<any[], any>, args: any[], value: any) {
        function isFunction(a: any): boolean {
            return typeof a === "function";
        }
        
        let keys: any[] = [];
        args.forEach((a) => {
            if (!isFunction(a)) {
                keys.push(eq(a));
            } else {
                keys.push(a);
            }            
        })
        expectedCalls.set(keys, value);
    }
}